import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

public class Test {

	int BANKER = 1;

	int PLAYER = 2;

	JFrame mainFrame = new JFrame("Baccarat");

	JTextArea fieldResult = new JTextArea();

	JScrollPane scrollResult = new JScrollPane(fieldResult);

	JComboBox fieldMartingale = new JComboBox(new String[] { "1", "0" });

	JTextField fieldNhands = new JTextField("1000000");

	JTextField fieldBankersBeforeBetting = new JTextField("9");

	JTextField fieldPlayersBeforeBetting = new JTextField("9");

	JTextField fieldNumberOfConsecutiveBetting = new JTextField("7");

	JComboBox fieldStopAfterConsecutiveBettingLoss = new JComboBox(
			new String[] { "NO", "YES" });
	
	Chart chart;

	public static void main(String[] args) {
		Test test = new Test();
		if(args.length>0 && args[0].equals("-verbose"))
			test.initGUI(true);
		else
			test.initGUI(false);
	}

	public void initGUI(final boolean isVerbose) {

		mainFrame.setSize(800, 600);
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.setLayout(new GridLayout(3, 1));

		JLabel labelMartingale = new JLabel("Martingale");
		labelMartingale.setPreferredSize(new Dimension(100, 10));
		JLabel labelNhands = new JLabel("Number of Hands");
		labelNhands.setPreferredSize(new Dimension(100, 10));
		JLabel labelBankersBeforeBetting = new JLabel(
				"Number of Bankers Before Betting on Player");
		labelBankersBeforeBetting.setPreferredSize(new Dimension(200, 10));
		JLabel labelPlayersBeforeBetting = new JLabel(
				"Number of Players Before Betting on Banker");
		labelPlayersBeforeBetting.setPreferredSize(new Dimension(200, 10));
		JLabel labelNumberOfConsecutiveBetting = new JLabel(
				"Max Number of Consecutive Loss Betting");
		labelNumberOfConsecutiveBetting
				.setPreferredSize(new Dimension(200, 10));
		JLabel labelStopAfterConsecutiveBettingLoss = new JLabel(
				"Stop After Reach Max Consecutive Loss Betting");
		labelStopAfterConsecutiveBettingLoss.setPreferredSize(new Dimension(
				200, 10));

		JPanel panelInputParameter = new JPanel();
		panelInputParameter.setLayout(new GridLayout(7, 2, 10, 10));
		panelInputParameter.add(labelMartingale);
		panelInputParameter.add(fieldMartingale);
		panelInputParameter.add(labelNhands);
		panelInputParameter.add(fieldNhands);
		panelInputParameter.add(labelBankersBeforeBetting);
		panelInputParameter.add(fieldBankersBeforeBetting);
		panelInputParameter.add(labelPlayersBeforeBetting);
		panelInputParameter.add(fieldPlayersBeforeBetting);
		panelInputParameter.add(labelNumberOfConsecutiveBetting);
		panelInputParameter.add(fieldNumberOfConsecutiveBetting);
		panelInputParameter.add(labelStopAfterConsecutiveBettingLoss);
		panelInputParameter.add(fieldStopAfterConsecutiveBettingLoss);

		JButton simulate = new JButton("Simulate");
		simulate.setPreferredSize(new Dimension(100, 100));
		simulate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					process(1,isVerbose);
				} catch (NumberFormatException e1) {
					JOptionPane.showMessageDialog(mainFrame,
							"Please put number 0-9 only", "information",
							JOptionPane.INFORMATION_MESSAGE);
				}
			}
		});
		JPanel panelButton = new JPanel();
		panelButton.setLayout(new GridLayout(1, 1));
		panelButton.add(simulate);

		JPanel panelResult = new JPanel();
		scrollResult.setPreferredSize(new Dimension(800, 30));
		fieldResult.setEditable(false);
		fieldResult.append("martingale" + "\t");
		fieldResult.append("bnker bef bet" + "\t");
		fieldResult.append("plyer bef bet" + "\t");
		fieldResult.append("max cons loss" + "\t");
		fieldResult.append("stop cons loss" + "\t");
		fieldResult.append("no hands" + "\t");
		fieldResult.append("BnkerStrkLoss" + "\t");
		fieldResult.append("PlyerStrkLoss" + "\t");
		fieldResult.append("BnkerStrkBet" + "\t");
		fieldResult.append("PlyerStrkBet" + "\t");
		fieldResult.append("Total L/P" + "\t\n");
		scrollResult
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		scrollResult
				.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		panelResult.setLayout(new GridLayout(1, 1));
		panelResult.add(scrollResult);

		mainFrame.getContentPane().add(panelInputParameter, BorderLayout.NORTH);
		mainFrame.getContentPane().add(panelButton, BorderLayout.CENTER);
		mainFrame.getContentPane().add(panelResult, BorderLayout.SOUTH);

		mainFrame.setLocationRelativeTo(null);
		mainFrame.pack();
		mainFrame.setVisible(true);
	}

	public void process(int ntrials, boolean isVerbose) {
		int[] total_payoff = new int[ntrials];
		int[][] profit = new int[ntrials][];
		double[] banker_streak_loss = new double[ntrials];
		double[] player_streak_loss = new double[ntrials];
		double[] banker_streak_betting_player = new double[ntrials];
		double[] player_streak_betting_banker = new double[ntrials];

		int BAD_PAYOFF = 999;
		int martingale = fieldMartingale.getSelectedItem().equals("") ? 0
				: Integer.parseInt((String) fieldMartingale.getSelectedItem());
		int numberBankersBeforeBetting = fieldBankersBeforeBetting.getText()
				.equals("") ? 0 : Integer.parseInt(fieldBankersBeforeBetting
				.getText());
		int numberPlayersBeforeBetting = fieldPlayersBeforeBetting.getText()
				.equals("") ? 0 : Integer.parseInt(fieldPlayersBeforeBetting
				.getText());
		int uniqueHand = fieldNhands.getText().equals("") ? 100 : Integer
				.parseInt(fieldNhands.getText());
		int maxConsecutiveLossBetting = fieldNumberOfConsecutiveBetting
				.getText().equals("") ? 4 : Integer
				.parseInt(fieldNumberOfConsecutiveBetting.getText());
		for (int i = 0; i < ntrials; i++) {
			int numberShoe =  (uniqueHand / 10) + 1;
			Shoe s = new Shoe(numberShoe, martingale, uniqueHand);			

			int payoff = s.chop_bet_mart(numberBankersBeforeBetting,
					numberPlayersBeforeBetting, maxConsecutiveLossBetting,
					fieldStopAfterConsecutiveBettingLoss.getSelectedItem()
							.equals("YES"), isVerbose);
			if (payoff != BAD_PAYOFF) {
				total_payoff[i] = payoff;
				profit[i] = s.getProfit();
				player_streak_loss[i] = s.getNumberOfPlayerStreakLoss();
				banker_streak_loss[i] = s.getNumberOfBankerStreakLoss();
				banker_streak_betting_player[i] = s
						.getNumberOfBankerStreakBettingPlayer();
				player_streak_betting_banker[i] = s
						.getNumberOfPlayerStreakBettingBanker();
			}
			
		}

		fieldResult.append(martingale + "\t");
		fieldResult.append(numberBankersBeforeBetting + "\t");
		fieldResult.append(numberPlayersBeforeBetting + "\t");
		fieldResult.append(maxConsecutiveLossBetting + "\t");
		fieldResult.append(fieldStopAfterConsecutiveBettingLoss
				.getSelectedItem()
				+ "\t");
		fieldResult.append(uniqueHand + "\t");
		fieldResult.append(banker_streak_loss[0] + "\t");
		fieldResult.append(player_streak_loss[0] + "\t");
		fieldResult.append(banker_streak_betting_player[0] + "\t");
		fieldResult.append(player_streak_betting_banker[0] + "\t");
		fieldResult.append(total_payoff[0] + "\t\n");

		if (banker_streak_betting_player[0] != 0
				|| player_streak_betting_banker[0] != 0
				|| banker_streak_loss[0] != 0
				|| player_streak_loss[0] != 0
				|| total_payoff[0] != 0) {
			chart = new Chart(profit[0], total_payoff[0]);
			chart.setLocationRelativeTo(null);
			chart.pack();
			chart.setVisible(true);
		} else
			JOptionPane.showMessageDialog(mainFrame,
					"Chart is not generated because no bet has been made", "information",
					JOptionPane.INFORMATION_MESSAGE);
	}
}
