
public class Hand {
	 
	int BAD_PAYOFF = 999;
	int BANKER_PAYOFF = 95;
	int PLAYER_PAYOFF = 100;
	int TIE_PAYOFF = 800;
	int LOSS_PAYOFF = -100;
	int BANKER = 1;
	int PLAYER = 2;
	int TIE = 3;
	private int[] banker = new int[3];  // array of card numbers for banker
	private int[] player = new int[3];  // array of card numbers for banker
	private int winner; // 1=banker, 2=player, 3-tie
	
// Constructors
	 
// Default constructor
	public Hand()
	{
	    for ( int i=0; i<3; i++ ) banker[i] = 0;
	    for ( int i=0; i<3; i++ ) player[i] = 0;
	    winner = 0;
	}
	 
// Copy constructor
	public Hand(Hand that)
	{
		copy(that);
	}
	 
// Overload assignment operator
	public void copy (Hand that)
	{
	    for ( int i=0; i<3; i++ ) banker[i] = that.banker[i];
	    for ( int i=0; i<3; i++ ) player[i] = that.player[i];
	    winner = that.winner;
	}
	 
// Set functions
	 
// Get functions
	public int get_payoff(int bet)
	{
	    // Make sure bet and winner are meaningful
	    if ( winner < 1 || winner > 3 ) return BAD_PAYOFF; 
	    if ( bet    < 1 || bet    > 3 ) return BAD_PAYOFF; 
	 
	    if ( bet == BANKER )
	    {
	        if ( winner == BANKER ) return BANKER_PAYOFF; 
	        if ( winner == PLAYER ) return LOSS_PAYOFF; 
	        if ( winner == TIE    ) return 0; 
	    }
	 
	    if ( bet == PLAYER )
	    {
	        if ( winner == BANKER ) return LOSS_PAYOFF; 
	        if ( winner == PLAYER ) return PLAYER_PAYOFF; 
	        if ( winner == TIE    ) return 0; 
	    }
	 
	    if ( bet == TIE )
	    {
	        if ( winner == BANKER ) return LOSS_PAYOFF; 
	        if ( winner == PLAYER ) return LOSS_PAYOFF; 
	        if ( winner == TIE    ) return TIE_PAYOFF; 
	    }
	 
	    // If this point is reached, something went wrong
	    return BAD_PAYOFF;
	}
	 
	public int get_number_of_cards()
	{
	    int count = 0;
	    for ( int i=0; i<3; i++ ) if ( banker[i] > 0 ) count++;
	    for ( int i=0; i<3; i++ ) if ( player[i] > 0 ) count++;
	    return count;
	}
	 
	public int get_number_of_picture_cards()
	{
	    int count = 0;
	    for ( int i=0; i<3; i++ ) 
	        if ( get_card_baccarat_value(banker[i]) == 0 ) count++;
	    for ( int i=0; i<3; i++ ) 
	        if ( get_card_baccarat_value(player[i]) == 0 ) count++;
	    return count;
	}
	 
	public int get_total_value_of_cards()
	{
	    int count = 0;
	    for ( int i=0; i<3; i++ ) 
	    {
	        int value = get_card_baccarat_value(banker[i]);
	        if ( value >= 0 ) count += value;
	    }
	    for ( int i=0; i<3; i++ ) 
	    {
	        int value = get_card_baccarat_value(player[i]);
	        if ( value >= 0 ) count += value;
	    }
	    return count;
	}
	 
// Write function
	public void write()
	{
	    for ( int i=0; i<3; i++ ) System.out.print(banker[i]+".");
	    for ( int i=0; i<3; i++ ) System.out.print(player[i]+".");
	    System.out.println(winner);	    
	}
	 
	public void write_baccarat_value()
	{
	    for ( int i=0; i<3; i++ ) 
	    {
	        if ( get_card_baccarat_value(banker[i]) >= 0 )
	        	System.out.print(get_card_baccarat_value(banker[i])+".");	            
	        else
	        	System.out.print(".");
	    }
	    for ( int i=0; i<3; i++ ) 
	    {
	        if ( get_card_baccarat_value(player[i]) >= 0 )
	        	System.out.print(get_card_baccarat_value(player[i])+".");
	        else
	        	System.out.print(".");
	    }
	    System.out.print(winner);
	    // cout << endl;
	}
	 
	/***************************************************************************
	 * Associated functions
	 **************************************************************************/
	 
	public String get_card_value(int card_number)
	{
	        // Check bounds
	        String value = "XX";
	        if ( card_number < 1  ) { return value; }
	        if ( card_number > 52 ) { return value; }
	 
	        // Determine card number (Ace,2,3, ...)
	        char number;
	        int x = card_number%13;
	        if      ( x == 0  ) number = 'K';
	        else if ( x == 1  ) number = 'A';
	        else if ( x == 2  ) number = '2';
	        else if ( x == 3  ) number = '3';
	        else if ( x == 4  ) number = '4';
	        else if ( x == 5  ) number = '5';
	        else if ( x == 6  ) number = '6';
	        else if ( x == 7  ) number = '7';
	        else if ( x == 8  ) number = '8';
	        else if ( x == 9  ) number = '9';
	        else if ( x == 10 ) number = '0';
	        else if ( x == 11 ) number = 'J';
	        else                number = 'Q';
	 
	        // Determine suit (H/S/D/C)
	        char suit;
	        int y = (card_number-1)/13;
	        if      ( y == 0 ) suit = 'H';
	        else if ( y == 1 ) suit = 'S';
	        else if ( y == 2 ) suit = 'D';
	        else               suit = 'C';
	 
	        // Form complete string representing card value
	        value=Integer.toString(number)+Integer.toString(suit);	        	 
	        return value;
	}
	 
	public int get_card_type(int card_number)
	{
	        // Check bounds
	        if ( card_number < 1  ) { return -1; }
	        if ( card_number > 52 ) { return -1; }
	 
	        // Return card type (1 to 13 for A to K)
	        return card_number%13 + 1;
	}
	 
	int get_card_baccarat_value(int card_number)
	{
	        // Check bounds
	        if ( card_number < 1  ) { return -1; }
	        if ( card_number > 52 ) { return -1; }
	 
	        // Return baccarat value
	        int baccarat_value = card_number%13;
	        if ( baccarat_value > 9 ) baccarat_value = 0;
	        return baccarat_value;
	}

	public int[] getBanker() {
		return banker;
	}

	public void setBanker(int[] banker) {
		this.banker = banker;
	}
	
	public void setBanker(int position, int value) {
		this.banker[position] = value;
	}

	public int[] getPlayer() {
		return player;
	}

	public void setPlayer(int[] player) {
		this.player = player;
	}
	
	public void setPlayer(int position, int value) {
		this.player[position] = value;
	}

	public int getWinner() {
		return winner;
	}

	public void setWinner(int winner) {
		this.winner = winner;
	}
	
	
}
