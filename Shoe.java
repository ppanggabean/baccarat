import java.util.Random;

public class Shoe {

	int MAX_CARDS = 10500000;

	int MAX_HANDS = 2630000;

	int BAD_PAYOFF = 999;

	int BANKER_PAYOFF = 95;

	int PLAYER_PAYOFF = 100;

	int TIE_PAYOFF = 800;

	int LOSS_PAYOFF = -100;

	int BANKER = 1;

	int PLAYER = 2;

	int TIE = 3;

	int ndecks; // number of full decks in shoe

	int nbets_made; // number of bets made

	int numberOfBankerStreakLoss; // number of banker streak n times and loss

	int numberOfPlayerStreakLoss; // number of player streak n times and loss

	int numberOfBankerStreakBettingPlayer; // number of banker streak n times

	// and start betting player

	int numberOfPlayerStreakBettingBanker; // number of player streak n times

	// and start betting banker

	int martingale = 0; // kind of martingale

	int numberUniqueHand = 0; // Number of unique hands

	int[] card = new int[MAX_CARDS]; // array of card numbers (up to 20

	// decks)

	int next; // next card index to be played

	Hand[] hand = new Hand[MAX_HANDS]; // array of Hands from this Shoe

	int nhands; // number of Hands in hand array

	int[] profit = new int[MAX_HANDS]; // profit

	// Constructors

	// Default constructor
	public Shoe() {
		ndecks = 0;
		next = 0;
		nhands = 0;
	}

	// Copy constructor
	public Shoe(Shoe that) {
		copy(that);
	}

	// Input constructor
	public Shoe(int _ndecks, int martingale, int numberUniqueHand) {
		ndecks = _ndecks;
		next = 52 * ndecks;
		this.martingale = martingale;
		this.numberUniqueHand = numberUniqueHand;

		if (ndecks <= 0)
			return;

		// Load decks into Shoe
		int[] deck = new int[53];
		for (int k = 0; k < ndecks; k++) {
			// Each deck is preshuffled in get_deck()
			get_deck(deck);
			for (int i = 0; i < 52; i++)
				card[52 * k + i] = deck[i];
		}

		// Initialize hands
		for (int i = 0; i < MAX_HANDS; i++)
			hand[i] = new Hand();

		// Shuffle Shoe
		shuffle();

		// Generate hands from this Shoe
		play_baccarat();
	}

	// Overload assignment operator
	public void copy(Shoe that) {
		ndecks = that.ndecks;
		next = that.next;
		for (int i = 0; i < MAX_CARDS; i++)
			card[i] = that.card[i];
		nhands = that.nhands;
		for (int i = 0; i < nhands; i++)
			hand[i] = that.hand[i];
	}

	// Set functions

	// Get functions

	public int count_number_of_cards_played_after_hand(int i) {
		int count = 0;
		for (int j = 0; j <= i; j++)
			count += hand[j].get_number_of_cards();
		return count;
	}

	public int count_banker_wins_so_far(int i) {
		int banker_wins = 0;
		for (int j = 0; j <= i; j++)
			if (hand[j].getWinner() == BANKER)
				banker_wins++;
		return banker_wins;
	}

	public int count_banker_wins_remaining(int i) {
		int banker_wins = 0;
		for (int j = i + 1; j < nhands; j++)
			if (hand[j].getWinner() == BANKER)
				banker_wins++;
		return banker_wins;
	}

	public int count_player_wins_so_far(int i) {
		int player_wins = 0;
		for (int j = 0; j <= i; j++)
			if (hand[j].getWinner() == PLAYER)
				player_wins++;
		return player_wins;
	}

	public int count_player_wins_remaining(int i) {
		int player_wins = 0;
		for (int j = i + 1; j < nhands; j++)
			if (hand[j].getWinner() == PLAYER)
				player_wins++;
		return player_wins;
	}

	public int count_cards_so_far(int i) {
		int count = 0;
		for (int j = 0; j <= i; j++)
			count += hand[j].get_number_of_cards();
		return count;
	}

	public int count_cards_remaining(int i) {
		int count = 0;
		for (int j = i + 1; j < nhands; j++)
			count += hand[j].get_number_of_cards();
		return count;
	}

	public int count_picture_cards_so_far(int i) {
		int picture = 0;
		for (int j = 0; j <= i; j++)
			picture += hand[j].get_number_of_picture_cards();
		return picture;
	}

	public int count_picture_cards_remaining(int i) {
		int picture = 0;
		for (int j = i + 1; j < nhands; j++)
			picture += hand[j].get_number_of_picture_cards();
		return picture;
	}

	public int count_percent_average_card_value_so_far(int i) {
		int k = i;
		if (i < 0)
			k = 0;
		if (i >= nhands - 1)
			k = nhands - 1;
		int total = 0;
		int count = 0;
		for (int j = 0; j <= k; j++) {
			total += hand[j].get_total_value_of_cards();
			count += hand[j].get_number_of_cards();
		}
		return (int) (100 * (double) total / (double) count);
	}

	public int count_percent_average_card_value_remaining(int i) {
		int k = i;
		if (i < 0)
			k = 0;
		if (i >= nhands - 1)
			return 0;
		int total = 0;
		int count = 0;
		for (int j = k + 1; j < nhands; j++) {
			total += hand[j].get_total_value_of_cards();
			count += hand[j].get_number_of_cards();
		}
		return (int) (100 * (double) total / (double) count);
	}

	public int count_forward_banker_wins(int i, int forward) {
		// Each banker win counts 1
		// Each tie counts 0
		// Each player win counts -1
		int k = i;
		if (i < 0)
			k = 0;
		if (k + forward >= nhands)
			return BAD_PAYOFF;
		int wins = 0;
		for (int j = k + 1; j <= (k + forward); j++) {
			int winner = hand[j].getWinner();
			if (winner == BANKER)
				wins++;
			if (winner == PLAYER)
				wins--;
		}
		return wins;
	}

	public int deal() {
		// If shoe is empty, return zero
		if (next == 0)
			return 0;

		// Otherwise, move one card down and return the top card
		--next;
		int top_card = card[next];
		return top_card;
	}

	public void shuffle() {
		for (int k = 0; k < 1000; k++) {
			// Get random pair of indices to swap
			Random rn = new Random();
			int x1 = rn.nextInt(next);
			int x2 = rn.nextInt(next);
			if (x1 == x2)
				continue;
			if (x1 < 0 || x2 < 0)
				continue;
			if (x1 >= next || x2 >= next)
				continue;

			// Swap those two
			int temp = card[x2];
			card[x2] = card[x1];
			card[x1] = temp;
		}
	}

	public void play_baccarat() {
		// Remember initial value of "next" to restore it at the end
		int initial_next = next;

		// Loop through the cards generating Hands
		int i = 0;
		while (true) {
			// Stop once Shoe is near end
			if (next < 22)
				break;

			// Deal first and second cards
			hand[i].setPlayer(0, deal());
			hand[i].setPlayer(1, deal());
			hand[i].setBanker(0, deal());
			hand[i].setBanker(1, deal());
			int player_card_1 = hand[i].getPlayer()[0];
			int player_card_2 = hand[i].getPlayer()[1];
			int banker_card_1 = hand[i].getBanker()[0];
			int banker_card_2 = hand[i].getBanker()[1];

			// Determine player and banker totals
			int player_card_value_1 = hand[i]
					.get_card_baccarat_value(player_card_1);
			int player_card_value_2 = hand[i]
					.get_card_baccarat_value(player_card_2);
			int banker_card_value_1 = hand[i]
					.get_card_baccarat_value(banker_card_1);
			int banker_card_value_2 = hand[i]
					.get_card_baccarat_value(banker_card_2);
			int player_total = player_card_value_1 + player_card_value_2;
			int banker_total = banker_card_value_1 + banker_card_value_2;
			player_total %= 10;
			banker_total %= 10;

			// Both players stand if either has 8 or 9 after two cards
			if (player_total >= 8 || banker_total >= 8) {
				// no more cards, both players stand
			} else {

				// Determine if player gets a third card
				int player_card_3 = -1; // default to -1 if no card is
				// dealt
				int player_card_value_3 = -1;
				if (player_total <= 5) {
					hand[i].setPlayer(2, deal());
					player_card_3 = hand[i].getPlayer()[2];
					player_card_value_3 = hand[i]
							.get_card_baccarat_value(player_card_3);
					player_total += player_card_value_3;
					player_total %= 10;
				}

				// Determine if bank gets a third card
				int banker_card_3 = -1; // default to -1 if no
				// card is dealt
				int banker_card_value_3 = -1;
				int banker_hits = 0; // Set to 1 if bank
				// should draw a card
				if (banker_total <= 2)
					banker_hits = 1;

				if (banker_total == 3)
					if (player_card_value_3 != 8)
						banker_hits = 1;

				if (banker_total == 4)
					if (player_card_value_3 != 0 && player_card_value_3 != 1
							&& player_card_value_3 != 8
							&& player_card_value_3 != 9)
						banker_hits = 1;

				if (banker_total == 5)
					if (player_card_value_3 != 0 && player_card_value_3 != 1
							&& player_card_value_3 != 2
							&& player_card_value_3 != 3
							&& player_card_value_3 != 8
							&& player_card_value_3 != 9)
						banker_hits = 1;

				if (banker_total == 6)
					if (player_card_value_3 == 6 || player_card_value_3 == 7)
						banker_hits = 1;

				if (banker_hits > 0) {
					hand[i].setBanker(2, deal());
					banker_card_3 = hand[i].getBanker()[2];
					banker_card_value_3 = hand[i]
							.get_card_baccarat_value(banker_card_3);
					banker_total += banker_card_value_3;
					banker_total %= 10;
				}

			} // End of "else"

			// Determine winner
			if (banker_total > player_total)
				hand[i].setWinner(BANKER);
			if (banker_total < player_total)
				hand[i].setWinner(PLAYER);
			if (banker_total == player_total)
				hand[i].setWinner(TIE);

			// Done with this hand, skip to the next one
			i++;

		} // End of while loop

		// Record number of hands played
		nhands = i;

		// Calculate net_banker and streak arrays
		int net_count = 0;
		int streak_count = 0;
		for (int j = 0; j < nhands; j++) {
			// If BANKER wins, extend or start new BANKER streak
			if (hand[j].getWinner() == BANKER) {
				net_count++;
				if (streak_count >= 0)
					streak_count++;
				else
					streak_count = 1;
			}

			// If PLAYER wins, extend or start new PLAYER streak
			if (hand[j].getWinner() == PLAYER) {
				net_count--;
				if (streak_count <= 0)
					streak_count--;
				else
					streak_count = -1;
			}

			// TIE has no effect on streaks
		}

		// Restore "next" to its original value
		next = initial_next;
	}

	/***************************************************************************
	 * Strategy functions
	 **************************************************************************/

	public int simple_bet(int bet) {
		if (nhands < 10 || hand == null)
			return BAD_PAYOFF;
		int total_payoff = 0;
		for (int i = 0; i < nhands; i++) {
			int payoff = hand[i].get_payoff(bet);
			if (payoff == BAD_PAYOFF)
				return BAD_PAYOFF;
			total_payoff += payoff;
		}
		return total_payoff;
	}

	// Bet on banker for entire shoe after waiting nwait hands
	public int counting_bet(int nwait, int lower_cutoff, int upper_cutoff) {
		// Check if Shoe or conditions are bad
		nbets_made = 0;
		if (nwait < 1 || nwait > 60)
			return BAD_PAYOFF;

		// Bet on banker
		int total_payoff = 0; // total payoff over all bets made
		int nbets = 0; // number of bets made
		int betting = 0; // 0 = no bet, 1 = bet on banker, 2 = bet on player
		int banker_wins = 0; // number of banker wins so far

		// Loop over hands, implementing betting rule
		for (int i = 0; i < nhands; i++) {
			// Tally outcome of current bet
			if (betting == BANKER) {
				total_payoff += hand[i].get_payoff(BANKER);
				nbets++;
			}
			if (betting == PLAYER) {
				total_payoff += hand[i].get_payoff(PLAYER);
				nbets++;
			}

			// Keep track of banker wins so far
			int winner = hand[i].getWinner();
			if (winner == BANKER)
				banker_wins++;
			int bw = (int) (100 * (double) banker_wins / (double) ((i + 1) * 0.4586));

			// Do not bet before waiting period is up
			if (i < nwait)
				continue;

			// Otherwise, bet on banker unless bw is extreme
			betting = BANKER;
			if (bw < lower_cutoff)
				betting = PLAYER;
			if (bw > upper_cutoff)
				betting = PLAYER;
		}
		nbets_made = nbets;
		if (nbets < 1)
			return BAD_PAYOFF;
		return (int) ((double) total_payoff / (double) nbets);
	}

	public int ascending_bet(int[] bet_size, int nbets, int bet) {
		// Check if Shoe or conditions are bad
		if (nhands < 10 || hand == null)
			return BAD_PAYOFF;
		if (nbets < 1 || nbets > 10)
			return BAD_PAYOFF;
		if (bet_size == null)
			return BAD_PAYOFF;

		// Apply ascending betting strategy while Shoe lasts
		int total_payoff = 0;
		int bet_number = 0;
		for (int i = 0; i < nhands; i++) {
			int payoff = hand[i].get_payoff(bet);
			if (payoff == BAD_PAYOFF)
				return BAD_PAYOFF;
			total_payoff += bet_size[bet_number] * payoff;
			if (payoff > 0 || bet_number == (nbets - 1)) {
				bet_number = 0;
				continue;
			}
			bet_number++;
		}
		return total_payoff;
	}

	public double chop_bet(int nchop, int bet) {
		// Check if Shoe or conditions are bad
		nbets_made = 0;
		if (nhands < 10 || hand == null)
			return BAD_PAYOFF;
		if (nchop < 1 || nchop > 10)
			return BAD_PAYOFF;

		// Wait for a streak of losses of length nchop, then bet until win
		int total_payoff = 0; // total payoff over all bets made
		int nbets = 0; // number of bets made
		int streak = 0; // number of consecutive losses
		int betting = 0; // flag to track if bet is on or not
		int winner;
		for (int i = 0; i < nhands; i++) {
			winner = hand[i].getWinner();
			// Record outcome if betting
			if (betting > 0) {
				total_payoff += hand[i].get_payoff(bet);
				if (winner != TIE)
					nbets++; // TIE is a push
			}

			// Determine if streak breaks or continues
			if (winner == bet)
				streak = 0;
			else if (winner != TIE)
				streak++;

			// Determine if betting next hand
			if (streak >= nchop)
				betting = 1;
			else
				betting = 0;
		}
		nbets_made = nbets;
		if (nbets < 1)
			return BAD_PAYOFF;
		return (double) total_payoff / (double) nbets;
	}

	public int chop_bet_mart(int banker, int player,
			int maxConsecutiveLossBetting,
			boolean stopAfterReachMaxConsecutiveLossBetting, boolean isVerbose) {
		// Check if Shoe or conditions are bad
		nbets_made = 0;
		numberOfBankerStreakLoss = 0;
		numberOfPlayerStreakLoss = 0;
		numberOfBankerStreakBettingPlayer = 0;
		numberOfPlayerStreakBettingBanker = 0;
		if (hand == null)
			return BAD_PAYOFF;

		// Wait for a streak of losses of length nchop, then bet until win
		// Look for banker chop 'banker' and then player chop 'player' in
		// alternation
		int bet = BANKER;// = firstStreakLosses; // flag whether next bet is
							// on banker or
		// player
		// int nchop; // 4 for player, 5 for banker
		int total_payoff = 0; // total payoff over all bets made
		int nbets = 0; // number of bets made
		// int streak = 0; // number of consecutive losses
		int betting = 0; // flag to track if bet is on or not
		int winner;
		int mart_counter = 0;
		int bet_size = 0;
		int tempBanker = 0;
		int tempPlayer = 0;
		String[] listBet = new String[] { "BANKER", "PLAYER", "TIE" };
		for (int i = 0; i < nhands; i++) {
			// Stop after unique hands
			if (i > numberUniqueHand) {
				profit[i] = -1;
				break;
			}

			winner = hand[i].getWinner();
			if (betting > 0) {
				bet_size = getMartingale(mart_counter);
				profit[i] = bet_size * hand[i].get_payoff(bet);
				total_payoff += profit[i];
				if (winner != TIE) // TIE is a push
				{
					nbets++;
					mart_counter++;
				}
				if (isVerbose)
					System.out.println("Hand " + i + "; Bet size: " + bet_size
							+ "; Bet on: " + listBet[bet - 1] + "; Result: "
							+ listBet[winner - 1]);
			} else if (isVerbose)
				System.out.println("Hand " + i
						+ "; Bet size: 0; Bet on: N/A; Result: "
						+ listBet[winner - 1]);

			if ((betting > 0 && winner == bet)
					|| mart_counter == maxConsecutiveLossBetting) {
				if (tempBanker == (banker + maxConsecutiveLossBetting - 1)
						&& banker != 0 && winner == BANKER)
					numberOfBankerStreakLoss++;
				else if (tempPlayer == (player + maxConsecutiveLossBetting - 1)
						&& player != 0 && winner == PLAYER)
					numberOfPlayerStreakLoss++;
				if (stopAfterReachMaxConsecutiveLossBetting
						&& mart_counter == maxConsecutiveLossBetting) {
					profit[i + 1] = -1;
					break;
				}
				mart_counter = 0;
				tempPlayer = 0;
				tempBanker = 0;
				betting = 0;
			}

			if (winner == BANKER) {
				tempBanker++;
				tempPlayer = 0;
			} else if (winner == PLAYER) {
				tempPlayer++;
				tempBanker = 0;
			}

			if (winner != TIE) {
				if (tempBanker == banker && banker != 0) {
					numberOfBankerStreakBettingPlayer++;
					betting = 1;
					bet = PLAYER;
				} else if (tempPlayer == player && player != 0) {
					numberOfPlayerStreakBettingBanker++;
					betting = 1;
					bet = BANKER;
				}
			}
		}
		nbets_made = nbets;
		profit[nhands] = -1;

		if (nbets < 1)
			return BAD_PAYOFF;
		return total_payoff;
	}

	/***************************************************************************
	 * Associated functions
	 **************************************************************************/

	public int getMartingale(int index) {
		if (index == 0)
			return 1;
		else {
			int result = 1;
			for (int i = 1; i <= index; i++)
				result = (result * 2) + martingale;
			return result;
		}
	}

	public void get_deck(int[] card) {
		if (card == null)
			return;
		for (int i = 0; i < 52; i++)
			card[i] = i + 1;
		for (int k = 0; k < 1000; k++) {
			// Get random pair of indices to swap
			Random rn = new Random();
			int x1 = rn.nextInt(52);
			int x2 = rn.nextInt(52);
			if (x1 == x2)
				continue;
			if (x1 < 0 || x2 < 0)
				continue;
			if (x1 >= 52 || x2 >= 52)
				continue;

			// Swap those two
			int temp = card[x2];
			card[x2] = card[x1];
			card[x1] = temp;
		}
		return;
	}

	public int[] getCard() {
		return card;
	}

	public void setCard(int[] card) {
		this.card = card;
	}

	public Hand[] getHand() {
		return hand;
	}

	public void setHand(Hand[] hand) {
		this.hand = hand;
	}

	public int getNdecks() {
		return ndecks;
	}

	public void setNdecks(int ndecks) {
		this.ndecks = ndecks;
	}

	public int getNext() {
		return next;
	}

	public void setNext(int next) {
		this.next = next;
	}

	public int getNhands() {
		return nhands;
	}

	public void setNhands(int nhands) {
		this.nhands = nhands;
	}

	public int getNbets_made() {
		return nbets_made;
	}

	public void setNbets_made(int nbets_made) {
		this.nbets_made = nbets_made;
	}

	public int[] getProfit() {
		return profit;
	}

	public void setProfit(int[] profit) {
		this.profit = profit;
	}

	public int getNumberOfBankerStreakBettingPlayer() {
		return numberOfBankerStreakBettingPlayer;
	}

	public void setNumberOfBankerStreakBettingPlayer(
			int numberOfBankerStreakBettingPlayer) {
		this.numberOfBankerStreakBettingPlayer = numberOfBankerStreakBettingPlayer;
	}

	public int getNumberOfBankerStreakLoss() {
		return numberOfBankerStreakLoss;
	}

	public void setNumberOfBankerStreakLoss(int numberOfBankerStreakLoss) {
		this.numberOfBankerStreakLoss = numberOfBankerStreakLoss;
	}

	public int getNumberOfPlayerStreakBettingBanker() {
		return numberOfPlayerStreakBettingBanker;
	}

	public void setNumberOfPlayerStreakBettingBanker(
			int numberOfPlayerStreakBettingBanker) {
		this.numberOfPlayerStreakBettingBanker = numberOfPlayerStreakBettingBanker;
	}

	public int getNumberOfPlayerStreakLoss() {
		return numberOfPlayerStreakLoss;
	}

	public void setNumberOfPlayerStreakLoss(int numberOfPlayerStreakLoss) {
		this.numberOfPlayerStreakLoss = numberOfPlayerStreakLoss;
	}

}
